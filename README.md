# Home automation backend and frontend

## Backend
The backend is based on this solution:
https://github.com/javiercbgr/webserver

I chose this solution because most of the part are well designed, the http calls are handling most of the cases, including headers, parameters.
What is changed:
- replaced static final ints with enums as best practice
- add standard logging (log4j) for better logging configuration
- added lombok library
- adding more test cases
- close down the connection in the Handler file, for quick restarts (otherwise the port is binded)
- the FileHttpResponse now can handle images as well (not only text files as before)
- in the http request body is also parsed as the part of the request (only text data)
- added a new service device controller, which handles smart home devices in rest style


## Frontend sources:

Curtain: http://talkerscode.com/webtricks/curtain-opening-effect-using-jquery-and-css.php
Lightbulb: https://www.w3schools.com/js/tryit.asp?filename=tryjs_lightbulb

The frontend shows all devices on the index page. The devices are dynamically loaded from the server`s in memory database.
You can change the status for all the devices with the proper button. If you change the status the client will call the backend to change the device status.
Currently 3 device types are supported:
- LIGHT
- TERMOSTAT
- CURTAIN

To add new device you should use the + button on the top right. You can define the device id and the type.
It uses responsive design so it should work fine on almost all resolutions. 


## Extensibility:
To add new device type you should add new subclass to the device interface. Each device has different DeviceType so it should be also extended with the new device.
You can specify certain validation rules in the DeviceInputValidator file (e.g. termostat min and max temperature)
On the frontend the addDeviceToGrid handles the creation of new devices, so it should be extended. 

## Install

mvn install

## Testing

### for unit tests
mvn test 

### to start the application:

java -jar webserver.jar 

by default it will start on localhost:8080, if you want to change that you can add parameters (-port -workers -documentroot).
The frontend also listens on localhost:8080.
