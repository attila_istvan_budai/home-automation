var devices = {};


function openDialog() {
    $('#dialog').show();
    $('#dialog').dialog();
}

function addNewDevice(){
    let e = document.getElementById("deviceOption");
    let deviceType = e.options[e.selectedIndex].value;
    let name = document.getElementById("newDeviceName").value;
    addDevice(name, deviceType);
    $('#dialog').hide();
}

function addDevice(id, deviceType) {
    let device = {};
    device.id = id;
    device.owner = "";
    device.type = deviceType;
    if (deviceType == "LIGHT") {
        device.on = false;
    } else if (deviceType == "CURTAIN") {
        device.closed = false;
    } else if (deviceType == "TERMOSTAT") {
        device.temperature = 24
    } else {
        return;
    }
    addDeviceCall(device);
    window.location.reload(false); 
}

function turnOnOff(imageId, btn) {
    let image = document.getElementById(imageId);
    let on = false;
    if (image.src.match("ONbulb")) {
        image.src = "images/OFFbulb.jpg";
        btn.className = "btn btn-light";
        btn.innerText = "On";

    }
    else {
        image.src = "images/ONbulb.jpg";
        btn.className = "btn btn-dark";
        btn.innerText = "Off";
        on = true
    }
    device = devices[imageId];
    device.on = on;
    updateDeviceCall(device);
}

function curtainsClosed(curtainId1, curtainId2) {
    console.log("close curtain");
    $("#" + curtainId1).animate({ width: 49.5 + '%' }, 500);
    $("#" + curtainId2).animate({ width: 49.5 + '%' }, 500);
}

function curtainsOpened(curtainId1, curtainId2) {
    console.log("open curtain");
    $("#" + curtainId1).animate({ width: 0 + '%' }, 500);
    $("#" + curtainId2).animate({ width: 0 + '%' }, 500);
}

function toggle_curtain(id, curtainId1, curtainId2, btn) {
    device = devices[id];
    if (device.closed) {
        device.closed = false;
    } else {
        device.closed = true;
    }
    setCurtainState(device, btn, curtainId1, curtainId2)
    updateDeviceCall(device);

}

function increaseTemp(ledid) {
    let led = document.getElementById(ledid);
    let temp = parseFloat(led.innerText)
    let newTemp = (temp + 0.1).toFixed(1)
    led.innerText = newTemp;

    device = devices[ledid];
    device.temperature = newTemp;
    updateDeviceCall(device);
}

function decreaseTemp(ledid) {
    let led = document.getElementById(ledid);
    let temp = parseFloat(led.innerText)
    let newTemp = (temp - 0.1).toFixed(1)
    led.innerText = newTemp;

    device = devices[ledid];
    device.temperature = newTemp;
    updateDeviceCall(device);
}

$(document).ready(function () {
    getDevices();
    $('#dialog').hide();
});

function getDevices() {
    let grid = document.getElementById('grid');
    grid.innerHTML = '';
    devices = {};
    fetch("http://localhost:8080/api/device")
        .then((resp) => resp.json())
        .then(function (data) {
            data.map(d => {
                console.log(d);
                devices[d.id] = d;
                addDeviceToGrid(d);
            })
        }).catch(function (error) {
            alert(error)
        });
}

function updateDeviceCall(device) {
    fetch("http://localhost:8080/api/device/" + device.id,
        {
            method: "PUT",
            body: JSON.stringify(device)
        })
        .then((resp) => resp.json())
        .then(function (data) {
            updateDevice(data);

        }).catch(function (error) {
            console.log(error)
            alert(error)
        });
}

function addDeviceCall(device) {
    fetch("http://localhost:8080/api/device/" + device.id,
        {
            method: "POST",
            body: JSON.stringify(device)
        })
        .then((resp) => resp.json())
        .then(function (data) {
            addDeviceToGrid(data);

        }).catch(function (error) {
            console.log(error)
            alert(error)
        });
}

function updateDevice(device) {
    if (device.type == "LIGHT") {
        let img = document.getElementById(device.id);
        let button = document.getElementById("btn_" + device.id);
        setLightState(device, img, button)
    } else if (device.type == "CURTAIN") {
        let button = document.getElementById("btn_" + device.id);
        setCurtainState(device, button, device.id + "left", device.id + "right")
    } else if (device.type == "TERMOSTAT") {
        let div = document.getElementById(device.id);
        setTermostatState(device, div);
    }


}


function addDeviceToGrid(d) {
    parentElement = document.getElementById('grid');
    outerDiv = document.createElement('div');
    innerDiv = document.createElement('div');
    innerDiv.className = "card-content"
    deviceName = document.createElement('h3');
    parentElement.appendChild(outerDiv);
    outerDiv.appendChild(innerDiv);
    innerDiv.appendChild(deviceName);
    deviceName.innerText = d.id;
    if (d.type == "LIGHT") {
        createLight(d, innerDiv);
    } else if (d.type == "CURTAIN") {
        createCurtain(d, innerDiv);
    } else if (d.type == "TERMOSTAT") {
        createTermostat(d, innerDiv);

    }


}

function createTermostat(d, innerDiv) {
    tempDiv = document.createElement('div');
    tempDiv.id = d.id;
    tempDiv.className = "termostat";
    setTermostatState(d, tempDiv);
    btnDiv = document.createElement('div');
    buttonInc = document.createElement('button');
    buttonInc.className = "btntermostat";
    buttonInc.onclick = function () {
        increaseTemp(d.id);
    };
    buttonInc.innerText = "^";
    buttonDec = document.createElement('button');
    buttonDec.className = "btntermostat";
    buttonDec.onclick = function () {
        decreaseTemp(d.id);
    };
    buttonDec.innerText = "v";
    innerDiv.appendChild(tempDiv);
    innerDiv.appendChild(btnDiv);
    btnDiv.appendChild(buttonDec);
    btnDiv.appendChild(buttonInc);
}

function setTermostatState(d, div) {
    div.innerText = d.temperature;
}

function createCurtain(d, innerDiv) {
    button = document.createElement('button');
    button.type = "button";
    button.className = "btn btn-light";
    button.id = "btn_" + d.id;
    button.onclick = function () {
        toggle_curtain(d.id, d.id + "left", d.id + "right", this);
    };
    imgleft = document.createElement('img');
    imgleft.id = d.id + "left";
    imgleft.className = "leftcurtain";
    imgleft.src = "images/curtain1.jpg";
    imgright = document.createElement('img');
    imgright.id = d.id + "right";
    imgright.className = "rightcurtain";
    imgright.src = "images/curtain2.jpg";
    innerDiv.appendChild(imgleft);
    innerDiv.appendChild(imgright);
    innerDiv.appendChild(button);
    setCurtainState(d, button, imgleft.id, imgright.id);
}

function setCurtainState(d, button, imgleftid, imgrightid) {
    console.log(d)
    if (d.closed) {
        button.innerText = "Open";
        button.className = "btn btn-dark";
        curtainsClosed(imgleftid, imgrightid);
    }
    else {
        button.innerText = "Close";
        button.className = "btn btn-light";
        curtainsOpened(imgleftid, imgrightid);
    }
}

function createLight(d, innerDiv) {
    button = document.createElement('button');
    button.type = "button";
    button.className = "btn btn-light";
    button.id = "btn_" + d.id;
    button.onclick = function () {
        turnOnOff(d.id, this);
    };
    img = document.createElement('img');
    img.id = d.id;
    img.className = "bulb";
    setLightState(d, img, button);
    innerDiv.appendChild(img);
    innerDiv.appendChild(button);
}

function setLightState(d, img, button) {
    if (d.on) {
        img.src = "images/ONbulb.jpg";
        button.innerText = "Off";
    }
    else {
        img.src = "images/OFFbulb.jpg";
        button.innerText = "On";
    }
}

