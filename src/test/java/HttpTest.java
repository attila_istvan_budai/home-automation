import com.gargoylesoftware.htmlunit.*;
import me.homework.server.WebServer;
import me.homework.server.apps.FileServingApp;
import org.junit.*;

import java.net.URL;

/**
 * Creates a new webserver with thread pooling and tests the requests.
 * 
 * Created by Mihail on 10/24/2015.
 * Modified by Javier on 12/05/2019.
 */
public class HttpTest {

    private Thread thread;
    private int port;
    WebClient webClient = new WebClient();
    WebServer server;

    @Before
    public void setUp() {
        port = 52052;
        server = new WebServer(port, 4, new FileServingApp("web/"));
        thread = new Thread(server);
        thread.start();

        webClient = new WebClient(BrowserVersion.CHROME);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);

    }

    @After
    public void tearDown() throws InterruptedException {

        server.interrupt();
        thread.join();
        webClient.close();
    }

    @Test
    public void testForbidden() throws Exception {
        Page unexpectedResult = webClient.getPage("http://127.0.0.1:" + port);
        Assert.assertEquals(403, unexpectedResult.getWebResponse().getStatusCode());
    }

    @Test
    public void testNotFound() throws Exception {
        Page unexpectedResult = webClient.getPage("http://127.0.0.1:" + port + "/dummy.html");
        Assert.assertEquals(404, unexpectedResult.getWebResponse().getStatusCode());
    }

    @Test
    public void testHeadForExisting() throws Exception {
        WebResponse response = webClient.loadWebResponse(new WebRequest(new URL("http://127.0.0.1:" + port + "/index.html"), HttpMethod.HEAD));
        Assert.assertEquals(200, response.getStatusCode());
    }

    @Test
    public void testHeadForNonExisting() throws Exception {
        WebResponse response = webClient.loadWebResponse(new WebRequest(new URL("http://127.0.0.1:" + port + "/dummy.html"), HttpMethod.HEAD));
        Assert.assertEquals(404, response.getStatusCode());
    }

    @Test
    public void testFolder() throws Exception {
        Page unexpectedResult = webClient.getPage("http://127.0.0.1:" + port + "/folder");
        Assert.assertEquals(403, unexpectedResult.getWebResponse().getStatusCode());
    }

    @Test
    public void testSuccess() throws Exception {
        Page successfulResult = webClient.getPage("http://127.0.0.1:" + port + "/folder/page.html");
        Assert.assertEquals(200, successfulResult.getWebResponse().getStatusCode());
    }

    @Test
    public void testOption() throws Exception {
        WebResponse response = webClient.loadWebResponse(new WebRequest(new URL("http://127.0.0.1:" + port + "/index.html"), HttpMethod.OPTIONS));
        Assert.assertEquals(501, response.getStatusCode());
    }


    @Test
    public void testDelete() throws Exception {
        WebResponse response = webClient.loadWebResponse(new WebRequest(new URL("http://127.0.0.1:" + port + "/index.html"), HttpMethod.DELETE));
        Assert.assertEquals(501, response.getStatusCode());
    }

}