package me.homework.server.exceptions;

public class InvalidDeviceInputException extends Exception {
    public InvalidDeviceInputException(String msg) {
        super(msg);
    }
}
