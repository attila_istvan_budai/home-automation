package me.homework.server.smarthome.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import me.homework.server.exceptions.DeviceIdNotFound;
import me.homework.server.exceptions.DuplicateKeyException;
import me.homework.server.smarthome.dao.DeviceDao;
import me.homework.server.smarthome.device.Device;

import java.util.List;

@AllArgsConstructor
public class DeviceService {

    private DeviceDao deviceDao;

    public List<Device> listAllDevice(){
        return deviceDao.listDevices();
    }

    public Device updateDevice(Device device) throws DeviceIdNotFound {
        return deviceDao.update(device);
    }

    public Device addDevice(Device device) throws DuplicateKeyException {
        return deviceDao.add(device);
    }



}
