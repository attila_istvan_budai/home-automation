package me.homework.server.smarthome.dao;

import me.homework.server.exceptions.DeviceIdNotFound;
import me.homework.server.exceptions.DuplicateKeyException;
import me.homework.server.smarthome.device.Device;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class DeviceDao {

    private final ConcurrentMap<String, Device> deviceMap;

    public DeviceDao() {
        this.deviceMap = new ConcurrentHashMap<>();
    }

    public Device add(Device device) throws DuplicateKeyException {
        if (null != deviceMap.putIfAbsent(device.getId(), device)) {
            throw new DuplicateKeyException();
        }
        return device;
    }

    public Device get(String id) {
        return deviceMap.get(id);
    }

    public Device update(Device device) throws DeviceIdNotFound {
        if (null == deviceMap.replace(device.getId(), device)) {
            throw new DeviceIdNotFound();
        }
        return deviceMap.get(device.getId());
    }

    public boolean delete(String id) {
        return null != deviceMap.remove(id);
    }

    public List<Device> listDevices() {
        return deviceMap.values()
                .stream()
                .collect(Collectors.toList());
    }
}
