package me.homework.server.smarthome.device;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Termostat implements Device {
    private String id;
    private String owner;
    private double temperature;


    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getOwner() {
        return owner;
    }

    @Override
    public DeviceType getType() {
        return DeviceType.TERMOSTAT;
    }
}
