package me.homework.server.smarthome.device;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Curtain.class, name = "CURTAIN"),
        @JsonSubTypes.Type(value = Light.class, name = "LIGHT"),
        @JsonSubTypes.Type(value = Termostat.class, name = "TERMOSTAT")})
public interface Device {

    public String getId();

    public String getOwner();

    public DeviceType getType();
}
