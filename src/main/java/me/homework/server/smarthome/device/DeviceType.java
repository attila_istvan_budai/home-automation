package me.homework.server.smarthome.device;

public enum DeviceType {
    LIGHT, CURTAIN, TERMOSTAT;
}
