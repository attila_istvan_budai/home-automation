package me.homework.server.workers;

import me.homework.server.WebServer;
import me.homework.server.apps.WebApp;
import me.homework.server.exceptions.BadRequestException;
import me.homework.server.exceptions.ConnectionClosedException;
import me.homework.server.helpers.PerformanceStats;
import me.homework.server.helpers.SocketConfig;
import me.homework.server.http.*;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Calendar;
import java.util.concurrent.Callable;
import java.nio.channels.SocketChannel;

/**
 * Handles a request, deciding based on its header wether to keep-alive
 * the socket connection.
 * 
 * Created by Mihail on 10/24/2015.
 * Modified by Javier on 12/05/2019.
 */
public class Handler implements Callable<SocketChannel> {

    final static Logger logger = Logger.getLogger(Handler.class);

    private WebApp app;
    private SocketChannel socketChannel;
    private InputStream in;
    private OutputStream out;

    /** Used to measure the request handling time. */
    private long creationTime;

    public Handler(SocketChannel socketChannel, WebApp app) {
        this.socketChannel = socketChannel;
        this.app = app;
        this.creationTime = System.currentTimeMillis();
    }

    @Override
    public SocketChannel call() {
        logger.info("*** Thread processing started! ***");
        HttpRequest request = null;
        Socket socket = null;
        try {

            socket = this.socketChannel.socket();
            socket.setSoTimeout(SocketConfig.socketTimeout);

            in = socket.getInputStream();
            out = socket.getOutputStream();
            logger.info("Parsing request...");
            request = HttpRequest.parse(in);

            if (request != null) {
                logger.info(request.getRequestLine() + " from " + socketChannel.getRemoteAddress());

                HttpResponse response = app.handle(request);

                // Build response headers.
                response.addHeader("Server", WebServer.SERVER_NAME);
                response.addHeader("Date", Calendar.getInstance().getTime().toString());
                if (request.isKeepAlive()) {
                    response.addHeader("Connection", "keep-alive");
                    response.addHeader("Keep-Alive", "timeout=" + (SocketConfig.keepAliveTime / 1000));
                } else {
                    response.addHeader("Connection", "close");
                }

                response.write(out);

                int handlingTime = (int) (System.currentTimeMillis() - this.creationTime);
                PerformanceStats.recordHandlingTime(handlingTime);
                logger.info("Request handled successfully!");
            } else {
                new RawHttpResponse(HttpStatus.NOT_IMPLEMENTED, "Server only accepts HTTP protocol").write(out);
            }
        } catch (BadRequestException e) {
            logger.info("Bad request.");
            new RawHttpResponse(HttpStatus.BAD_REQUEST, "Server only accepts HTTP protocol.").write(out);
        } catch (SocketTimeoutException e) {
            logger.info("Timeout while keeping thread aliving.");
        } catch (IOException e) {
            logger.info("Error in client's IO.");
        }  catch (ConnectionClosedException e) {
            logger.info("Connection closed by client.");
        } catch (Exception e) {
            logger.error(e.getStackTrace());
            e.printStackTrace();
        }finally {
            if (request == null || !request.isKeepAlive()) {
                try {
                    in.close();
                } catch (IOException e) {
                    logger.info("Error while closing input stream.");
                }
                try {
                    out.close();
                } catch (IOException e) {
                    logger.info("Error while closing output stream.");
                }
                try {
                    socket.close();
                } catch (IOException e) {
                    logger.info("Error while closing socket.");
                }
                return null;
            } else {
                // Return for socket reuse.
                return this.socketChannel;
            }
        }
    }

}
