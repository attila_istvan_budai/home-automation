package me.homework.server.http;

import org.apache.log4j.Logger;

import java.io.*;

/**
 * HTTP Response class used in communication with the client.
 * This class contains the data that will be sent to the client,
 * including status line, headers, and response body.
 *
 * Created by Mihail on 10/24/2015.
 */
public class RawHttpResponse extends HttpResponse {

    final static Logger logger = Logger.getLogger(RawHttpResponse.class);

    private String content;

    public RawHttpResponse(HttpStatus status, String content) {
        super(status);
        this.content = content;
    }

    /**
     * This function writes the HTTP response to an output stream.
     *
     * @param out the target {@link OutputStream} for writing
     */
    public void write(OutputStream out) {
        try {
            BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(out));
            writer.write(getResponseLine());
            writer.write("\r\n");

            for (String key: headers.keySet()) {
                writer.write(key + ":" + headers.get(key));
                writer.write("\r\n");
            }
            writer.write("\r\n");

            writer.write(content);

            writer.flush();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

}

