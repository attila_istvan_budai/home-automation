package me.homework.server.http;


public enum HttpMethod {
    OPTIONS, GET, HEAD, TRACE, POST, PUT, DELETE, CONNECT
}
