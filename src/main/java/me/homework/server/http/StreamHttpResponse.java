package me.homework.server.http;

import org.apache.log4j.Logger;

import java.io.*;

/**
 * HTTP Response class used in communication with the client.
 * This class contains the data that will be sent to the client,
 * including status line, headers, and response body.
 *
 * Created by Mihail on 10/24/2015.
 * Modified by Attila Budai on 02/24/2020.
 */
public class StreamHttpResponse extends HttpResponse {

    final static Logger logger = Logger.getLogger(StreamHttpResponse.class);

    /**
     * Stream to be sent to the user.
     */
    private InputStream inputStream;

    public StreamHttpResponse(HttpStatus status, InputStream inputStream) {
        super(status);
        this.inputStream = inputStream;
    }

    /**
     * This function writes the HTTP response to an output stream.
     *
     * @param out the target {@link OutputStream} for writing
     */
    public void write(OutputStream out) {
        try {
            BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(out));
            writer.write(getResponseLine());
            writer.write("\r\n");

            for (String key: headers.keySet()) {
                writer.write(key + ":" + headers.get(key));
                writer.write("\r\n");
            }
            writer.write("\r\n");

            if (inputStream != null) {
                BufferedReader reader = new BufferedReader(
                    new InputStreamReader(inputStream));
                char[] buffer = new char[1024];
                int read;
                while ((read = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, read);
                }
                reader.close();
            }

            writer.flush();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
}

