package me.homework.server.http;

import lombok.Getter;
import lombok.Setter;
import me.homework.server.exceptions.BadRequestException;
import me.homework.server.exceptions.ConnectionClosedException;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.net.SocketTimeoutException;

/**
 * Contains all the information included in a request.
 * Contains a static method {@link #parse(InputStream)} that creates an 
 * {@link HttpRequest} by reading and parsing the data from the socket.
 * 
 * Created by Mihail on 10/24/2015.
 * Modified by Attila Budai on 02/24/2020.
 */
@Getter
@Setter
public class HttpRequest {

    final static Logger logger = Logger.getLogger(HttpRequest.class);

    /** Input stream associated with the source socket. */
    private InputStream inputStream;

    /** Request line containing protocol version, URI, and request method. */
    private String requestLine;

    private HttpMethod method;
    private String uri;
    private String version;

    /** Request headers. */
    private HashMap<String, String> headers = new HashMap<String, String>();

    /** Query parameters. */
    private HashMap<String, String> params = new HashMap<String, String>();

    /** URL path part. */
    private String path;

    /** URL query part. */
    private String query;

    /** Flag for keep-alive requests. */
    private boolean keepAlive = false;

    private String body;


    /** 
    * HTTP/1.0 needs an explicit header "Connection: keep-alive" while
    * HTTP/1.1 has keep-alive by default unless receiving "Connection: close".
    * https://tools.ietf.org/html/rfc7230#section-6.3
    */
    private static boolean containsKeepAlive(String version, HashMap<String, String> headers) {
        String connectionHeader = headers.getOrDefault("Connection", "").trim();
        boolean containsConnectionClose = connectionHeader.equalsIgnoreCase("close");
        boolean containsKeepAlive = connectionHeader.equalsIgnoreCase("keep-alive");
        return ("HTTP/1.0".equals(version) && containsKeepAlive) || ("HTTP/1.1".equals(version) && !containsConnectionClose);
    }

    /**
     * This method creates a new {@link HttpRequest} with the data read form the
     * {@link #inputStream}. Will throw exceptions in case of errors.
     *
     * @param inputStream The stream to read from.
     * @return A new instance of {@link HttpRequest} containing information from
     *     the request.
     *
     * @throws BadRequestException If the request is not properly formatted.
     * @throws ConnectionClosedException If there is an error while reading 
     *     data.
     * @throws IOException Could be due to the client closing the connection or 
     *     other IO problem.
     * @throws SocketTimeoutException If the socket timeouts while reading.
     */
    public static HttpRequest parse(InputStream inputStream) 
                                        throws BadRequestException,
                                               ConnectionClosedException,
                                               IOException,
                                               SocketTimeoutException {
        try {
            HttpRequest request = new HttpRequest();
            request.inputStream = inputStream;
            BufferedReader reader = new BufferedReader(
                new InputStreamReader(inputStream));

            // Read HTTP method, URI and version.
            request.requestLine = reader.readLine();
            if (request.requestLine == null) {
                throw new ConnectionClosedException();
            }
            String[] requestLineParts = request.requestLine.split(" ", 3);
            request.method = HttpMethod.valueOf(requestLineParts[0]);
            request.uri = requestLineParts[1];
            request.version = requestLineParts[2];

            // Read headers.
            readHeaders(request, reader);

            parseBody(request, reader);


            logger.info(request.uri);
            // Process URI requested.
            String[] uriParts = request.uri.split("\\?", 2);
            if (uriParts.length == 2) {
                request.path = uriParts[0];
                request.query = uriParts[1];

                String[] keyValuePairs = request.query.split("&");
                for (String keyValuePair : keyValuePairs) {
                    String[] keyValue = keyValuePair.split("=", 2);
                    if (keyValue.length == 2) {
                        request.params.put(keyValue[0], keyValue[1]);
                    }
                }
            } else {
                request.path = request.uri;
                request.query = "";
            }

            if (containsKeepAlive(request.version, request.headers)) {
                logger.info("Detected keep-alive on client connection.");
                request.keepAlive = true;
            }

            return request;
        } catch(ConnectionClosedException  | SocketTimeoutException e) {
            throw e;
        } catch (IOException e){
            throw e;
        }  catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException();
        }
    }

    private static void readHeaders(HttpRequest request, BufferedReader reader) throws IOException {
        String line = reader.readLine();
        while (!line.equals("")) {
            String[] lineParts = line.split(":", 2);
            if (lineParts.length == 2) {
                request.headers.put(lineParts[0], lineParts[1]);
            }
            line = reader.readLine();
        }
    }

    private static void parseBody(HttpRequest request, BufferedReader reader) throws IOException {
        int size = 0;
        if(request.headers.containsKey("Content-Length")) {
            size = Integer.parseInt(request.headers.get("Content-Length").trim());
        }
        StringBuffer buffer = new StringBuffer();
        for(int i=0; i<size; i++){
            char a=(char)(reader.read());
            buffer.append(a);
        }
        request.body = buffer.toString();
    }
}
