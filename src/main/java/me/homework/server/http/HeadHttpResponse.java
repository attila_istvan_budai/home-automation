package me.homework.server.http;

import lombok.RequiredArgsConstructor;
import org.apache.log4j.Logger;

import java.io.*;

/**
 * HttpResponse extension that only writes headers.
 *
 * Created by Mihail on 10/24/2015.
 * Modified by Attila Budai on 02/24/2020.
 */

public class HeadHttpResponse extends FileHttpResponse {

    final static Logger logger = Logger.getLogger(HeadHttpResponse.class);

    public HeadHttpResponse(HttpStatus status, File inputFile) {
        super(status, inputFile);
    }

    /**
     * This function writes the HTTP response to an output stream.
     *
     * @param out the target {@link OutputStream} for writing
     */
    public void write(OutputStream out) {
        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
            writeResponseAndHeaders(writer);
            writer.write("\r\n");

            writer.flush();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
}

