package me.homework.server.http;

import lombok.RequiredArgsConstructor;
import me.homework.Main;
import org.apache.log4j.Logger;

import java.io.*;

/**
 * HttpResponse extension that writes an empty response.
 * 
 * Created by Mihail on 10/24/2015.
 * Modified by Attila Budai on 02/24/2020.
 */
public class EmptyHttpResponse extends HttpResponse {

    final static Logger logger = Logger.getLogger(EmptyHttpResponse.class);

    public EmptyHttpResponse(HttpStatus status) {
        super(status);
    }

    /**
     * This function writes the HTTP response to an output stream.
     *
     * @param out the target {@link OutputStream} for writing
     */
    public void write(OutputStream out) {
        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));

            writeResponseAndHeaders(writer);
            writer.write("Content-Type: text/html; charset=utf-8\r\n");
            writer.write("Content-Length: 0\r\n");
            writer.write("\r\n");
            
            writer.flush();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

}

