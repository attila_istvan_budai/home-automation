package me.homework.server.http;

import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class JsonResponse extends HttpResponse {

        final static Logger logger = Logger.getLogger(RawHttpResponse.class);

        private String content;

        public JsonResponse(HttpStatus status, String content) {
            super(status);
            this.content = content;
        }

        /**
         * This function writes the HTTP response to an output stream.
         *
         * @param out the target {@link OutputStream} for writing
         */
        public void write(OutputStream out) {
            try {
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(out));
                writeResponseAndHeaders(writer);
                writer.write("Content-Type: application/json; charset=utf-8\r\n");
                writer.write("Content-Length: " +content.length() + "\r\n");
                writer.write("\r\n");

                writer.write(content);

                writer.flush();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }

    }