package me.homework.server.http;

import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * HttpResponse extensions that sends a file to the client (e. g. html).
 *
 * Created by Mihail on 10/24/2015.
 * Modified by Attila Budai on 02/24/2020.
 */
public class FileHttpResponse extends HttpResponse {

    final static Logger logger = Logger.getLogger(FileHttpResponse.class);

    /**
     * File to be sent to the user.
     */
    private File inputFile;

    public FileHttpResponse(HttpStatus status, File inputFile) {
        super(status);
        this.inputFile = inputFile;

        try {
            this.setContentType();
            this.setContentLength();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * This function writes the HTTP response to an output stream.
     *
     * @param out the target {@link OutputStream} for writing
     */
    public void write(OutputStream out) {
        logger.info("Writing file output");
        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
            writeResponseAndHeaders(writer);
            writer.write("\r\n");
            writer.flush();
            if (inputFile != null) {
                byte[] content = Files.readAllBytes(inputFile.toPath());
                out.write(content);
            }
            out.flush();




        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }


    }

    private void setContentType() throws IOException {
        Path source = Paths.get(this.inputFile.toURI());
        String contentType = Files.probeContentType(source);
        if (contentType != null) {
            headers.put("Content-Type", contentType);
        }
    }

    private void setContentLength() throws IOException {
        headers.put("Content-Length", String.valueOf(this.inputFile.length()));
    }
}

