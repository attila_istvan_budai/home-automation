package me.homework.server.apps;

import me.homework.server.exceptions.InvalidDeviceInputException;
import me.homework.server.smarthome.device.Device;
import me.homework.server.smarthome.device.DeviceType;
import me.homework.server.smarthome.device.Termostat;

public class DeviceInputValidator {

    public void validateDevice(Device device) throws InvalidDeviceInputException {
        if(device.getType() == DeviceType.TERMOSTAT){
            Termostat term = (Termostat)device;
            if(term.getTemperature()>30 || term.getTemperature()<16){
                throw new InvalidDeviceInputException("Termostat temperature is invalid");
            }
        }


    }
}
