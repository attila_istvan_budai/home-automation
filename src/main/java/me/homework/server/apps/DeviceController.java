package me.homework.server.apps;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import me.homework.server.exceptions.DeviceIdNotFound;
import me.homework.server.exceptions.DuplicateKeyException;
import me.homework.server.exceptions.InvalidDeviceInputException;
import me.homework.server.http.*;
import me.homework.server.smarthome.dao.DeviceDao;
import me.homework.server.smarthome.device.Curtain;
import me.homework.server.smarthome.device.Device;
import me.homework.server.smarthome.device.Light;
import me.homework.server.smarthome.device.Termostat;
import me.homework.server.smarthome.service.DeviceService;
import org.apache.log4j.Logger;

import java.io.IOException;

import static me.homework.server.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static me.homework.server.http.HttpStatus.NOT_IMPLEMENTED;

@AllArgsConstructor
public class DeviceController implements WebApp {

    final static Logger logger = Logger.getLogger(DeviceController.class);

    private ObjectMapper mapper = new ObjectMapper();

    DeviceInputValidator validator;

    DeviceService service;

    public DeviceController(){
        validator = new DeviceInputValidator();
        service = new DeviceService(new DeviceDao());
        try {
            service.addDevice(new Light("lamp1", "", false));
            service.addDevice(new Termostat("term1", "", 24));
            service.addDevice(new Curtain("curtain1", "", false));
        } catch (DuplicateKeyException e) {
            e.printStackTrace();
        }
    }

    @Override
    public HttpResponse handle(HttpRequest request) {
        logger.info("Handling API query " + request.getPath());
        if(request.getPath().equals("/api/device") && request.getMethod() == HttpMethod.GET){
            try {
                return new JsonResponse(HttpStatus.OK, mapper.writeValueAsString(service.listAllDevice()));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

        }
        else if(request.getPath().startsWith("/api/device/") && request.getMethod() == HttpMethod.PUT){
            String[] parts = request.getPath().split("//");
            if(parts.length == 3){
                String id = parts[2];

            }
            String errorMsg = "";
            try {
                logger.info("Update device to " + request.getBody());
                Device d = mapper.readValue(request.getBody(), Device.class);
                validator.validateDevice(d);
                String message = mapper.writeValueAsString(service.updateDevice(d));
                logger.info("Updated device " + message);

                return new JsonResponse(HttpStatus.OK, message);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                errorMsg = "json is not correctly formatted";
            } catch(IOException e){
                e.printStackTrace();
                errorMsg = "io exception happened";
            } catch (DeviceIdNotFound deviceIdNotFound) {
                deviceIdNotFound.printStackTrace();
                errorMsg = "the device with the id has not been found";
            } catch(InvalidDeviceInputException invalidInput){
                invalidInput.printStackTrace();
                errorMsg = invalidInput.getMessage();
            }
            return  new RawHttpResponse(INTERNAL_SERVER_ERROR, errorMsg);

        } else if(request.getPath().startsWith("/api/device/")  && request.getMethod() == HttpMethod.POST){
            String errorMsg = "";
            try {
                Device d = mapper.readValue(request.getBody(), Device.class);
                validator.validateDevice(d);
                String message = mapper.writeValueAsString(service.addDevice(d));

                return new JsonResponse(HttpStatus.OK, message);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                errorMsg = "json is not correctly formatted";
            } catch(IOException e){
                e.printStackTrace();
                errorMsg = "io exception happened";
            } catch (DuplicateKeyException e) {
                e.printStackTrace();
                errorMsg = "a device with this id is already existing";
            }catch(InvalidDeviceInputException invalidInput){
                invalidInput.printStackTrace();
                errorMsg = invalidInput.getMessage();
            }
            return  new RawHttpResponse(INTERNAL_SERVER_ERROR, errorMsg);
        }
        return  new EmptyHttpResponse(NOT_IMPLEMENTED);
    }
}
