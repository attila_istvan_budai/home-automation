package me.homework.server.apps;

import lombok.RequiredArgsConstructor;
import me.homework.server.http.*;

import javax.imageio.ImageIO;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static me.homework.server.http.HttpMethod.*;
import static me.homework.server.http.HttpStatus.*;

/**
 * A static file server application.
 * <p>
 * Created by Mihail on 10/24/2015.
 */
@RequiredArgsConstructor
public class FileServingApp implements WebApp {

    private final String documentRoot;

    @Override
    public HttpResponse handle(HttpRequest request) {

        String path = request.getPath();

        HttpResponse response;

        switch (request.getMethod()) {
            case GET:
                Path requestedFile = Paths.get(documentRoot, path);
                if (Files.exists(requestedFile)) {
                    if (Files.isDirectory(requestedFile)) {
                        response = new EmptyHttpResponse(FORBIDDEN);
                    } else {
                        response = new FileHttpResponse(HttpStatus.OK, getFile(path));
                    }
                } else {
                    response = new EmptyHttpResponse(NOT_FOUND);
                }
                break;
            case TRACE:
                response = new StreamHttpResponse(HttpStatus.OK, request.getInputStream());
                break;
            case HEAD:
                if (Files.exists(Paths.get(documentRoot, path))) {
                    response = new HeadHttpResponse(OK, getFile(path));
                } else {
                    response = new EmptyHttpResponse(NOT_FOUND);
                }
                break;
            case OPTIONS:
                response = new EmptyHttpResponse(NOT_IMPLEMENTED);
                break;
            default:
                response = new EmptyHttpResponse(NOT_IMPLEMENTED);
                break;
        }

        return response;
    }

    private File getFile(String path) {
        return new File(Paths.get(documentRoot, path).toString());
    }
}
