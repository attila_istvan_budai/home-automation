package me.homework.server.apps;

import me.homework.server.http.HttpRequest;
import me.homework.server.http.HttpResponse;
import org.apache.log4j.Logger;

public class WebAppRouter implements WebApp {

    final static Logger logger = Logger.getLogger(WebAppRouter.class);

    FileServingApp fileServingApp;
    DeviceController deviceController;



    public WebAppRouter(String documentRoot){
        fileServingApp = new FileServingApp(documentRoot);
        deviceController = new DeviceController();
    }

    @Override
    public HttpResponse handle(HttpRequest request) {
        logger.info("Handling request on path " + request.getPath());
        if(request.getPath().startsWith("/api/device")){
            return deviceController.handle(request);
        } else {
            return fileServingApp.handle(request);
        }
    }
}
