package me.homework;

import me.homework.server.WebServer;
import me.homework.server.apps.FileServingApp;
import me.homework.server.apps.WebAppRouter;
import org.apache.log4j.Logger;

/**
 * Created by Mihail on 10/24/2015.
 * Modified by Javier on 14/05/2019.
 */
public class Main {

    final static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        logger.info("server started");
        if (args.length == 3) {
            new WebServer(Integer.parseInt(args[0]), Integer.parseInt(args[1]), new WebAppRouter(args[2])).run();
        } else {
            new WebServer(8080, 100, new WebAppRouter("web/")).run();
        }
    }

}
